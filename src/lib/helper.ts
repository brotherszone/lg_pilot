import { UploadFile } from "antd/lib/upload/interface";

const getBase64 = (file: File) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

const getSumOfFileSize = (fileList: Array<UploadFile>) => {
    let total = 0;
    for (let i = 0; i < fileList.length; i++) {
        const file = fileList[i];
        total += file.size;
    }
    return total / 1024 / 1024;
}

const getFileExstension = (filename: string) => {
    if (filename) {
        const splitFilenameArr = filename.split('.');
        return splitFilenameArr[splitFilenameArr.length - 1];
    }
    return '';
}

const getFileName = (filename: string) => {
    if (filename) {
        const splitFilenameArr = filename.split('.');
        return splitFilenameArr[0];
    }
    return '';
}

const getFileExtensions = (fileList: Array<UploadFile>) => {
    let extensionsArr: string[] = [];
    for (let i = 0; i < fileList.length; i++) {
        const curFile = fileList[i];
        const curExtension = getFileExstension(curFile.name);
        extensionsArr.push(curExtension);
    }
    return extensionsArr;
}

const validateEmail = (email: string) => {
    let re = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return re.test(email);
}

export {
    getBase64,
    getSumOfFileSize,
    getFileExstension,
    getFileExtensions,
    getFileName,
    validateEmail
};

