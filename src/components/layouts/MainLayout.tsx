import { Col, Layout, Row } from 'antd';
import React from 'react';
import { IMainLayout } from '../../@types';

const MainLayout = (props: IMainLayout) => {

    const { Header, Content } = Layout;

    return (
        <Layout className="container">
            <Row justify="center">
                <Col xs={24} sm={20} md={16} lg={12} className="wrapper">
                    <Header className='header'>
                        <img className='header__signal' src='/assets/images/background_signal.png' alt="Background Signal" />
                        <img className='logo_text' src='/assets/images/logo_singlex_text.png' alt="Logo" />
                    </Header>
                    <Content>
                        {props.children}
                    </Content>
                </Col>
            </Row>
        </Layout>
    )
}

export default MainLayout;