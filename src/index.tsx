import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './index.css';
import './assets/scss/custom.scss'
import './assets/scss/uikit.scss'
import 'antd/dist/antd.css';
import reportWebVitals from './reportWebVitals';
import HomeScreen from './containers/home';
import ResultScreen from './containers/result';
import Page404 from './containers/errors/page404';

ReactDOM.render(
	<BrowserRouter>
		<Switch>
			<Route path="/result" component={ResultScreen} />
			<Route path="/" component={HomeScreen} />
			<Route path="/*" component={Page404} />
		</Switch>
	</BrowserRouter>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
