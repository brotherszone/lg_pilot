import { Button, Radio, RadioChangeEvent, message, Spin } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import { Formik, Form, Field } from 'formik';
import React from 'react';
import MainLayout from '../../components/layouts/MainLayout';
import { getFileExtensions, getSumOfFileSize, validateEmail } from '../../lib/helper';
import { getPresignedUrls, submitFormData, uploadFile } from './api';
import ChooseImageStep from './ChooseImageStep';

interface State {
    allowSendEmail: string,
    email: string,
    companyName: string,
    title: string,
    description: string,
    filesSize: number,
    fileList: Array<UploadFile>,
    fileIds: number[],
    loading: boolean,
    uploadToken: string[]
}

interface IFormData {
    companyName: string,
    title: string,
    description: string,
    email: string | null
}

interface IBody {
    companyName: string,
    title: string,
    description: string,
    email: string | null,
    uploadToken?: string[],
    fileIds: number[]
}

const MAX_FILES_SIZE_MB = 15;

export default class HomeScreen extends React.Component<any, State> {

    state: State = {
        allowSendEmail: 'true',
        email: '',
        companyName: '',
        title: '',
        description: '',
        filesSize: 0,
        fileList: [],
        fileIds: [],
        loading: false,
        uploadToken: []
    }

    onToggleSendEmail = (e: RadioChangeEvent) => {
        this.setState({ allowSendEmail: e.target.value });
    }

    onInputEmail = (val: string) => {
        this.setState({ email: val });
    }

    onSubmit = (data: IFormData) => {
        let isValidForm = this.formValidation(data);
        isValidForm && this.processPresignedUrls(data);
    }

    formValidation = (data: IFormData) => {
        if (data.companyName.length === 0) {
            message.error('Company name is required. Please try again!');
            return false;
        }

        if (data.companyName.length >= 200) {
            message.error('Company name is must less than 200 char. Please try again!');
            return false;
        }

        if (data.title.length === 0) {
            message.error('Title is required. Please try again!');
            return false;
        }

        if (data.title.length >= 200) {
            message.error('Title is must less than 200 char. Please try again!');
            return false;
        }

        if (data.description.length >= 4000) {
            message.error('Description is must less than 4000 char. Please try again!');
            return false;
        }

        if (this.state.filesSize > MAX_FILES_SIZE_MB) {
            message.error('File sizes must smaller than 15MB. Please try again!');
            return false;
        }

        if (this.state.allowSendEmail === 'true' &&
            data?.email?.length === 0) {
            message.error('Email is required. Please try again!');
            return false;
        }

        if (data.email && (
            this.state.allowSendEmail === 'true' &&
            !validateEmail(data?.email))) {
            message.error('Email is invalid. Please try again!');
            return false;
        }

        return true;
    }

    handleChooseImage = (fileList: Array<UploadFile>) => {
        let totalSize = getSumOfFileSize(fileList);
        this.setState({ filesSize: totalSize, fileList });
    }

    processPresignedUrls = async (data: any) => {
        try {
            this.setState({ loading: true });
            let extensionArr = getFileExtensions(this.state.fileList);
            if (this.state.fileList.length === 0) {
                this.submitForm(data);
            } else {
                let response = await getPresignedUrls(extensionArr);
                if (response?.statusCode !== 200) {
                    message.error('Cannot get presigned url(s). Please try again!');
                    return;
                }
                let uploadToken = response.data.uploadToken;
                let preSignedFiles = response.data.files;
                let promises = [];
                let fileIds = [];

                for (let i = 0; i < preSignedFiles.length; i++) {
                    let currentFile = this.state.fileList[i];
                    const preSignedFile = preSignedFiles[i];
                    fileIds.push(preSignedFile.id);
                    promises.push(uploadFile(preSignedFile.uploadUrl, currentFile.originFileObj));
                }

                this.setState({ uploadToken: [uploadToken], fileIds });

                await Promise.all(promises)
                    .then(() => this.submitForm(data))
                    .catch((error: Error) => {
                        console.log(`ProcessPresignedUrls_Error: `, error);
                        message.error('Upload was unsuccessful. Please try again!');
                    });
            }
        } catch (error) {
            this.setState({ loading: false });
            message.error(`Something went wrong:${error}!`);
        }

    }

    submitForm = async (data: IFormData) => {
        if (this.state.allowSendEmail !== 'true') {
            data.email = null;
        }

        let body: IBody = {
            ...data,
            fileIds: this.state.fileIds,
        }

        if (this.state.fileList.length > 0) {
            body = {
                ...body,
                uploadToken: this.state.uploadToken
            }
        }

        let response = await submitFormData(body);
        this.setState({ loading: false });
        if (response?.statusCode === 200) {
            this.props.history.push('/result');
        } else {
            message.error('Server error, please report to admin!');
        }
    }

    render() {
        return (
            <MainLayout>
                <div className='header__content'>
                    <span className='header-title header-title--bold600'>
                        고객 PainPoint<span className='header-title header-title--bold500'>를</span>
                    </span>
                    <span className='header-title header-title--bold500'>듣습니다</span>
                    <span className='header-sub-title header-sub-title--marginTop'>LG OO의 제품과 서비스를 이용하며 느끼는</span>
                    <span className='header-sub-title'>고객님의 진짜 경험을 들려주세요.</span>
                </div>
                <Spin size="large" spinning={this.state.loading} delay={500}>
                    <Formik initialValues={{
                        companyName: this.state.companyName,
                        title: this.state.title,
                        description: this.state.description,
                        email: this.state.email,
                    }} onSubmit={this.onSubmit}>
                        <Form>
                            <Field className="custom-input" placeholder="* 고객님의 회사를 입력하세요" id="companyName" name="companyName" />
                            <Field className="custom-input" placeholder="* 제목을 입력하세요" id="title" name="title" />
                            <div className="title-container">
                                <img src="/assets/icons/ic_step_one.png" alt="Step 1" className="icon-number" />
                                <span className="title">불편해요/칭찬해요</span>
                            </div>
                            <div className="content-container divider">
                                <Field id="description" name="description">
                                    {({ field }: any) => (
                                        <textarea {...field} className="custom-textarea"
                                            rows={5}
                                            placeholder="자유롭게 의견을 입력하세요."
                                            id="description"
                                            name="description"></textarea>
                                    )}
                                </Field>
                            </div>
                            <div className="title-container">
                                <img src="/assets/icons/ic_step_two.png" alt="Step 2" className="icon-number" />
                                <span className="title">파일 첨부</span>
                                <span className="size-container">
                                    {
                                        this.state.filesSize > MAX_FILES_SIZE_MB ?
                                            <span className="invalid-size">{this.state.filesSize.toFixed(1)}</span>
                                            : `${this.state.filesSize.toFixed(1)}`
                                    }
                                / {MAX_FILES_SIZE_MB}MB
                            </span>
                            </div>
                            <div className="content-container">
                                <ChooseImageStep handleChooseImage={this.handleChooseImage} />
                            </div>
                            <div className="title-container">
                                <img src="/assets/icons/ic_step_three.png" alt="Step 3" className="icon-number" />
                                <span className="title">개선알림</span>
                            </div>
                            <div className="content-container">
                                <b className="step3-label">개인정보의 수집 및 이용동의</b>
                                <div className="send-email-warning-container">
                                    <span className="text">
                                        답변을 전달드리기 위해 <span className="underline-text">E-mail 주소</span>를 수집합니다. <br />
                                    개인정보의 수집 및 이용에 대한 동의를 거부할 수 있으며 이 경우 개선 결과를 전달하지 않습니다. 개인정보 수집 및 이용에 동의하시겠습니까?
                                </span>
                                    <div className="copy-btn-container">
                                        <div className="copy-btn">
                                            개인정보수집이용동의서 보기
                                <img src="/assets/icons/ic_copy.png" alt="Copy" className="copy-icon" />
                                        </div>
                                    </div>
                                </div>
                                <Radio.Group className="send-mail-radio-group" onChange={this.onToggleSendEmail} value={this.state.allowSendEmail}>
                                    <Radio value="true">동의함</Radio>
                                    <Radio value="false">동의하지않음</Radio>
                                </Radio.Group>
                                {this.state.allowSendEmail === 'true' &&
                                    <div className="email-input-container">
                                        <b>개선진행사항을 전달받을 이메일을 입력하세요</b>
                                        <Field type="text" placeholder="이메일" id="email" name="email" />
                                    </div>
                                }
                            </div>
                            <div className="footer">
                                <Button htmlType="submit">제출</Button>
                            </div>
                        </Form>
                    </Formik>
                </Spin>
            </MainLayout>
        )
    }

}