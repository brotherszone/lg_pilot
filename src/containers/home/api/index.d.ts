
interface IPresignedItem {
    id: number,
    url: string,
    uploadUrl: string,
    key: string,
    status: string,
    report_file_id: any,
    created_at: string
}

export interface IPresignedData {
    files: IPresignedItem[],
    uploadToken: string
}

export interface IPresignedResponse {
    statusCode: number,
    data: IPresignedData
}

export interface IRequestData {
    companyName: string,
    title: string,
    description: string,
    email: string | null,
    fileIds: number[]
}

interface ISubmitData {
    message: string
}

export interface ISubmitResponse {
    statusCode: number,
    data: ISubmitData
}