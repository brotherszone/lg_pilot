import { IPresignedResponse, IRequestData, ISubmitResponse } from './index.d';
import axios from "axios";

const { REACT_APP_PILOT_API } = process.env;
const ERROR_PREFIX = 'Home - Api - ';

export async function getPresignedUrls(extensionArr: string[]) {
    try {
        let paramsUrl = '[';
        for (let i = 0; i < extensionArr.length; i++) {
            const extension = extensionArr[i];
            paramsUrl += `"${extension}",`;
        }
        paramsUrl = paramsUrl.substring(0, paramsUrl.length - 1);
        paramsUrl += ']';

        let path = `${REACT_APP_PILOT_API}get-presigned-urls?files=${paramsUrl}`;

        const { data } = await axios.get<IPresignedResponse>(path);
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}getPresignedUrls`, error.message);
    }
}

export async function uploadFile(url: string, file: File) {
    try {
        const { data } = await axios.put(url, file, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': file.type
            }
        });
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}uploadFile`, error.message);
    }
}

export async function submitFormData(requestData: IRequestData) {
    try {
        let path = `${REACT_APP_PILOT_API}submit-report-form`;
        const { data } = await axios.post<ISubmitResponse>(path, requestData, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        return data;
    } catch (error) {
        console.log(`${ERROR_PREFIX}submitFormData`, error.message);
    }
}