import { UploadFile } from 'antd/lib/upload/interface';
import { getFileExstension, getFileName } from '../../../lib/helper';

interface Props {
    onRemove: (item: UploadFile) => void,
    file: UploadFile
}

const FileItem = ({ file, onRemove }: Props) => {

    const getFileIcon = (fileType: string) => {
        switch (fileType) {
            case 'image/png':
            case 'image/jpeg':
            case 'image/gif':
            case 'image/heif':
                return (
                    <img src="/assets/icons/ic_image.png" aria-hidden alt="Image icon" className="file-icon" />
                );
            case 'application/pdf':
                return (
                    <img src="/assets/icons/ic_pdf.png" alt="Pdf icon" className="file-icon" />
                );
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            case 'application/vnd.ms-excel':
                return (
                    <img src="/assets/icons/ic_excel.png" alt="Excel icon" className="file-icon" />
                );
            case 'text/plain':
                return (
                    <img src="/assets/icons/ic_txt.png" alt="Text icon" className="file-icon" />
                );
            case 'video/mp4':
            case 'video/webm':
            case 'video/ogg':
            case 'video/x-ms-wmv':
                return (
                    <img src="/assets/icons/ic_video.png" alt="Text icon" className="file-icon" />
                );
            default:
                return (
                    <img src="/assets/icons/ic_other.png" alt="Other icon" className="file-icon" />
                )
        }
    }

    const getFileSize = (size: number) => {
        const fileSize = size / 1024 / 1024;
        return `${fileSize.toFixed(1)}MB`;
    }

    return (
        <div className="file-item">
            {getFileIcon(file.type)}
            <div className="file-description truncate">
                <span className="file-name">{getFileName(file.name)}</span>
                <span className="file-extension">.{getFileExstension(file.name)}</span>
                <br />
                <span className="file-size">{getFileSize(file.size)}</span>
            </div>
            <button className="btn-remove" type="button" onClick={() => onRemove(file)}>
                <img src="/assets/icons/ic_remove.png" alt="Remove icon" className="file-icon" />
            </button>
        </div>
    )
}

export default FileItem;