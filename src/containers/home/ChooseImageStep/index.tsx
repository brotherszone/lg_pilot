import { useState } from 'react';
import PropTypes from 'prop-types';
import { message, Upload } from 'antd';
import { UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import FileItem from './FileItem';

const MAX_FILE_SIZE_MB = 5;

interface Props {
    handleChooseImage: (fileList: Array<UploadFile>) => void
}

interface State {
    fileList: Array<UploadFile>,
}

const ChooseImageStep = ({ handleChooseImage }: Props) => {

    const [state, setState] = useState<State>({
        fileList: [],
    });

    const handleChange = (info: UploadChangeParam<UploadFile>) => {
        const { fileList } = info;
        handleChooseImage(fileList);
        setState({ ...state, fileList });
    };

    const beforeUpload = (file: File) => {
        const isValidFileSize = file.size / 1024 / 1024 < MAX_FILE_SIZE_MB;
        if (!isValidFileSize) {
            message.error(`File must smaller than ${MAX_FILE_SIZE_MB}MB!`);
            return Upload.LIST_IGNORE;
        }
        return false;
    }

    const onRemoveFile = (item: UploadFile) => {
        let filterFileList = state.fileList;
        filterFileList = filterFileList.filter(d => d.uid !== item.uid);
        setState({ ...state, fileList: filterFileList });
        handleChooseImage(filterFileList);
    }

    return (
        <>
            <div className="step-2-container">
                <Upload
                    beforeUpload={beforeUpload}
                    fileList={state.fileList}
                    onChange={handleChange}
                    multiple={true}
                >
                    <div className="step2-content-container">
                        <img src="/assets/icons/ic_add_file.png" alt="Icon camera" className="camera-icon" />
                        <div className="pick-img-description">
                            파일을 첨부하세요. <br />파일 한 개당 최대 <b className="size">5MB</b> 까지 가능합니다.
                        </div>
                    </div>
                </Upload>
            </div>
            {state.fileList.length > 0 &&
                state.fileList.map((item, index) => {
                    return (
                        <FileItem key={index} file={item} onRemove={() => onRemoveFile(item)} />
                    )
                })
            }
        </>
    )
}

ChooseImageStep.propTypes = {
    handleChooseImage: PropTypes.func.isRequired
}

export default ChooseImageStep;