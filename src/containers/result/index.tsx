import React from 'react';
import MainLayout from '../../components/layouts/MainLayout';

export default class ResultScreen extends React.Component {

    render() {
        return (
            <MainLayout>
                <div className='result-container'>
                    <p>고객의 소리를 전달하였습니다</p>
                    <p>소중한 의견 감사합니다</p>
                </div>
            </MainLayout>
        )
    }

}